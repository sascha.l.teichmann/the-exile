// This is Free Software under GNU Affero General Public License v >= 3.0
// without warranty, see README.md and license for details.
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// License-Filename: LICENSES/AGPL-3.0.txt
//
// Copyright (C) 2021 by Intevation GmbH
//
// Author(s):
// * Sascha L. Teichmann <sascha.teichmann@intevation.de>

package main

import (
	"database/sql"
	"embed"
	"html/template"
	"net/http"
	"strings"

	"gitlab.com/sascha.l.teichmann/the-exile/pkg/exile"
	"gitlab.com/sascha.l.teichmann/the-exile/pkg/log"
)

type pksFlags int

const (
	pksFlagsFingerprint = pksFlags(1) << iota
	pksFlagsHash
	pksFlagsExact
	pksFlagsMr
)

type pks struct {
	cfg  *exile.Config
	db   *sql.DB
	tmpl *template.Template
}

//go:embed pks-tmpl
var pksTmplFS embed.FS

func newPks(cfg *exile.Config, db *sql.DB) (*pks, error) {

	p := pks{cfg: cfg, db: db}
	var err error

	if p.tmpl, err = template.ParseFS(pksTmplFS, "pks-tmpl/*.html"); err != nil {
		return nil, err
	}

	return &p, nil
}

func (p *pks) bind(mux *http.ServeMux) {
	mux.HandleFunc("/", p.index)
	mux.HandleFunc("/pks", p.index)
	mux.HandleFunc("/pks/index", p.index)
	mux.HandleFunc("/pks/lookup", p.lookup)
	mux.HandleFunc("/pks/add", p.add)
	mux.HandleFunc("/pks/stats", p.stats)
	mux.HandleFunc("/pks/upload", p.upload)
	mux.HandleFunc("/pks/about", p.about)
}

func parsePksFlags(req *http.Request) pksFlags {
	var flags pksFlags
	if req.FormValue("fingerprint") != "" {
		flags |= pksFlagsFingerprint
	}
	if req.FormValue("hash") != "" {
		flags |= pksFlagsHash
	}
	if req.FormValue("exact") != "" {
		flags |= pksFlagsExact
	}
	if req.FormValue("mr") != "" {
		flags |= pksFlagsMr
	}
	return flags
}

func (pk pksFlags) String() string {
	var flags []string
	if pk&pksFlagsFingerprint != 0 {
		flags = append(flags, "fingerprint")
	}
	if pk&pksFlagsHash != 0 {
		flags = append(flags, "hash")
	}
	if pk&pksFlagsExact != 0 {
		flags = append(flags, "exact")
	}
	if pk&pksFlagsMr != 0 {
		flags = append(flags, "mr")
	}
	return "[" + strings.Join(flags, "|") + "]"
}

func (p *pks) render(rw http.ResponseWriter, tmpl string, arg interface{}) {
	if err := p.tmpl.ExecuteTemplate(rw, tmpl, arg); err != nil {
		log.Warnf("failed to render '%s': %v\n", tmpl, err)
	}
}

func (p *pks) index(rw http.ResponseWriter, req *http.Request) {
	// TODO: Implement me!

	p.render(rw, "index.html", nil)
}

func (p *pks) lookup(rw http.ResponseWriter, req *http.Request) {
	// TODO: Implement me!

	var (
		search = req.FormValue("search")
		op     = req.FormValue("op")
		flags  = parsePksFlags(req)
	)

	log.Infof("search: '%s'\n", search)
	log.Infof("op: '%s'\n", op)
	log.Infof("flags: %s\n", flags)

	p.render(rw, "lookup.html", nil)
}

func (p *pks) add(rw http.ResponseWriter, req *http.Request) {
	// TODO: Implement me!

	p.render(rw, "add.html", nil)
}

func (p *pks) stats(rw http.ResponseWriter, req *http.Request) {
	// TODO: Implement me!

	p.render(rw, "stats.html", nil)
}

func (p *pks) upload(rw http.ResponseWriter, req *http.Request) {
	// TODO: Implement me!

	p.render(rw, "upload.html", nil)
}

func (p *pks) about(rw http.ResponseWriter, req *http.Request) {

	p.render(rw, "about.html", struct{ Version string }{Version: exile.Version})
}
