// This is Free Software under GNU Affero General Public License v >= 3.0
// without warranty, see README.md and license for details.
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// License-Filename: LICENSES/AGPL-3.0.txt
//
// Copyright (C) 2021 by Intevation GmbH
//
// Author(s):
// * Sascha L. Teichmann <sascha.teichmann@intevation.de>

package main

import (
	"context"
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"strings"

	"gitlab.com/sascha.l.teichmann/the-exile/pkg/database"
	"gitlab.com/sascha.l.teichmann/the-exile/pkg/exile"
	"gitlab.com/sascha.l.teichmann/the-exile/pkg/log"
)

func usage() {
	out := flag.CommandLine.Output()
	fmt.Fprintf(out, "Usage of %s:\nVersion %s\n", os.Args[0], exile.Version)
	fallbacks := strings.Join(exile.DefaultConfigPaths, ", ")
	fmt.Fprintf(out, "Configuration fallback files: %s\n", fallbacks)
	flag.PrintDefaults()
}

func main() {

	var (
		configFile string
		migrate    bool
	)

	flag.Usage = usage

	flag.StringVar(&configFile, "c", "", "configuration file (shorthand)")
	flag.StringVar(&configFile, "config", "", "configuration file")
	flag.BoolVar(&migrate, "m", false, "do database migration (shorthand)")
	flag.BoolVar(&migrate, "migrate", false, "do database migration")

	flag.Parse()

	var cfg *exile.Config
	var err error

	if configFile != "" {
		cfg, err = exile.TryConfigs(configFile)
	} else {
		cfg, err = exile.TryConfigs()
	}

	if err != nil {
		log.Fatalf("parsing config failed: %v\n", err)
	}

	log.SetLogLevel(cfg.Logging.Level)
	if err := log.SetupLog(cfg.Logging.File, 0600); err != nil {
		log.Fatalf("opening log file failed: %v\n", err)
	}
	defer log.ShutdownLog()

	if migrate {
		if err := database.Migrate(cfg); err != nil {
			log.Fatalf("migration of database failed: %v\n", err)
		}
		return
	}

	db, err := database.DBFromConfig(cfg)
	if err != nil {
		log.Fatalf("Opening database failed: %v\n", err)
	}
	defer db.Close()

	if err := database.CheckMigrationVersion(db); err != nil {
		log.Errorf("Schema check failed: %v\n", err)
	}

	p, err := newPks(cfg, db)
	if err != nil {
		log.Fatalf("cannot create PKS: %v\n", err)
	}

	mux := http.NewServeMux()

	p.bind(mux)

	addr := fmt.Sprintf("%s:%d", cfg.PKS.Host, cfg.PKS.Port)

	pksServer := http.Server{
		Addr:    addr,
		Handler: mux,
	}

	done := make(chan struct{})

	go func() {
		defer close(done)
		log.Infof("PKS server listening at %s\n", addr)
		serve := pksServer.ListenAndServe
		if cfg.TLS.CertFile != "" && cfg.TLS.KeyFile != "" {
			serve = func() error {
				log.Infoln("using TLS")
				return pksServer.ListenAndServeTLS(cfg.TLS.CertFile, cfg.TLS.KeyFile)
			}
		}
		if err := serve(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("failed to start PKS server: %v\n", err)
		}
	}()

	sigChan := make(chan os.Signal)
	signal.Notify(sigChan, os.Interrupt)

	<-sigChan
	pksServer.Shutdown(context.Background())
	<-done

	log.Infoln("PKS server finished")
}
