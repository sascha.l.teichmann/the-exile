# Makefile to simplify the build of all components of the exile
#
# This Makefile accepts variables to customice the build:
#
# RELEASE : If st to 1 a release version is build,
#           the default is to build a development snapshot.
#
# Authors: Sascha Wilde <wilde@intevation.de>

RELEASE = 0

binaries = exiled
buldflags =

ifneq (${RELEASE},1)
buildflags += -ldflags '-X gitlab.com/sascha.l.teichmann/the-exile/pkg/exile.Version=dev-'$$(git rev-parse --short HEAD)
endif

.PHONY: clean exiled

all: $(binaries)

exiled:
	go build $(buildflags) ./cmd/exiled

clean:
	-for f in $(binaries) ; do \
	  [ -f $$f ] && rm $$f ; \
	done
