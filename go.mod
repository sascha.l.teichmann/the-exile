module gitlab.com/sascha.l.teichmann/the-exile

go 1.16

require (
	github.com/lib/pq v1.10.1 // indirect
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	gopkg.in/ini.v1 v1.62.0 // indirect
)
