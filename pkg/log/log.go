// This is Free Software under GNU Affero General Public License v >= 3.0
// without warranty, see README.md and license for details.
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// License-Filename: LICENSES/AGPL-3.0.txt
//
// Copyright (C) 2021 by Intevation GmbH
//
// Author(s):
// * Sascha L. Teichmann <sascha.teichmann@intevation.de>

package log

import (
	lg "log"
	"os"
	"strings"
	"sync"
	"sync/atomic"
)

type LogLevel uint32

const (
	TraceLogLevel = LogLevel(iota)
	DebugLogLevel
	InfoLogLevel
	WarnLogLevel
	ErrorLogLevel
	FatalLogLevel
)

var (
	logLevel  = uint32(InfoLogLevel)
	logFileMu sync.Mutex
	logFile   *os.File
)

func SetupLog(filename string, perm os.FileMode) error {
	f, err := os.OpenFile(filename, os.O_APPEND|os.O_CREATE|os.O_WRONLY, perm)
	if err != nil {
		return err
	}
	logFileMu.Lock()
	defer logFileMu.Unlock()
	if logFile != nil {
		logFile.Close()
	}
	logFile = f
	lg.SetOutput(logFile)
	return nil
}

func ShutdownLog() {
	logFileMu.Lock()
	defer logFileMu.Unlock()
	if logFile != nil {
		logFile.Close()
		logFile = nil
	}
	lg.SetOutput(os.Stderr)
}

func ParseLogLevel(s string) LogLevel {
	switch strings.ToLower(s) {
	case "trace":
		return TraceLogLevel
	case "debug":
		return DebugLogLevel
	case "info":
		return InfoLogLevel
	case "warn":
		return WarnLogLevel
	case "error":
		return ErrorLogLevel
	case "fatal":
		return FatalLogLevel
	default:
		return InfoLogLevel
	}
}

func (level LogLevel) String() string {
	switch level {
	case TraceLogLevel:
		return "trace"
	case DebugLogLevel:
		return "debug"
	case InfoLogLevel:
		return "info"
	case WarnLogLevel:
		return "warn"
	case ErrorLogLevel:
		return "error"
	case FatalLogLevel:
		return "fatal"
	default:
		return "unknown"
	}
}

func GetLogLevel() LogLevel {
	return LogLevel(atomic.LoadUint32(&logLevel))
}

func SetLogLevel(level LogLevel) {
	atomic.StoreUint32(&logLevel, uint32(level))
}

func Tracef(fmt string, args ...interface{}) {
	if TraceLogLevel >= GetLogLevel() {
		lg.Printf("[TRACE] "+fmt, args...)
	}
}

func Traceln(s string) {
	if TraceLogLevel >= GetLogLevel() {
		lg.Println("[TRACE] " + s)
	}
}

func Debugf(fmt string, args ...interface{}) {
	if DebugLogLevel >= GetLogLevel() {
		lg.Printf("[DEBUG] "+fmt, args...)
	}
}

func Debugln(s string) {
	if DebugLogLevel >= GetLogLevel() {
		lg.Println("[DEBUG] " + s)
	}
}

func Infof(fmt string, args ...interface{}) {
	if InfoLogLevel >= GetLogLevel() {
		lg.Printf("[INFO] "+fmt, args...)
	}
}

func Infoln(s string) {
	if InfoLogLevel >= GetLogLevel() {
		lg.Println("[INFO] " + s)
	}
}

func Warnf(fmt string, args ...interface{}) {
	if WarnLogLevel >= GetLogLevel() {
		lg.Printf("[WARN] "+fmt, args...)
	}
}

func Warnln(s string) {
	if WarnLogLevel >= GetLogLevel() {
		lg.Println("[WARN] " + s)
	}
}

func Errorf(fmt string, args ...interface{}) {
	if ErrorLogLevel >= GetLogLevel() {
		lg.Printf("[ERROR] "+fmt, args...)
	}
}

func Errorln(s string) {
	if ErrorLogLevel >= GetLogLevel() {
		lg.Println("[ERROR] " + s)
	}
}

func Fatalf(fmt string, args ...interface{}) {
	if FatalLogLevel >= GetLogLevel() {
		lg.Fatalf("[FATAL] "+fmt, args...)
	}
}

func Fatalln(s string) {
	if FatalLogLevel >= GetLogLevel() {
		lg.Fatalln("[FATAL] " + s)
	}
}
