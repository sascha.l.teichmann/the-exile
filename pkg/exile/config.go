// This is Free Software under GNU Affero General Public License v >= 3.0
// without warranty, see README.md and license for details.
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// License-Filename: LICENSES/AGPL-3.0.txt
//
// Copyright (C) 2021 by Intevation GmbH
//
// Author(s):
// * Sascha L. Teichmann <sascha.teichmann@intevation.de>
// * Sascha Wilde <sascha.wilde@intevation.de>

package exile

import (
	"errors"
	"os"
	"path/filepath"

	"github.com/mitchellh/go-homedir"
	"gopkg.in/ini.v1"

	"gitlab.com/sascha.l.teichmann/the-exile/pkg/log"
)

const (
	loggingSection = "logging"
	pksSection     = "pks"
	tlsSection     = "tls"
	dbSection      = "db"
)

type PKS struct {
	Host string
	Port uint
}

var DefaultPKS = PKS{
	Host: "localhost",
	Port: 11371,
}

type TLS struct {
	CertFile string
	KeyFile  string
}

var DefaultTLS = TLS{
	CertFile: "",
	KeyFile:  "",
}

type Logging struct {
	File  string
	Level log.LogLevel
}

var DefaultLogging = Logging{
	File:  "exile.log",
	Level: log.InfoLogLevel,
}

type DB struct {
	Host     *string
	Port     *uint
	Name     string
	User     *string
	Password *string
	SSLMode  *string
}

var DefaultDB = DB{
	Host:     nil,
	Port:     nil,
	Name:     "exiledb",
	User:     nil,
	Password: nil,
	SSLMode:  nil,
}

type parser interface {
	parse(*ini.File) error
}

type Config struct {
	Logging Logging
	PKS     PKS
	TLS     TLS
	DB      DB
}

var DefaultConfigPaths = []string{
	".exilerc",
	"~/.exile/exilerc",
	"/etc/exilerc",
}

func ExpandPath(path string) (string, error) {
	path, err := homedir.Expand(path)
	if err != nil {
		return path, err
	}
	if !filepath.IsAbs(path) {
		exepath, err := os.Executable()
		if err != nil {
			return path, err
		}
		return filepath.Dir(exepath) + "/" + path, nil
	}
	return path, nil
}

func TryConfigs(user ...string) (*Config, error) {
	user = append(user, DefaultConfigPaths...)
	return LoadConfig(user...)
}

func (p *PKS) parse(cfg *ini.File) error {

	section, err := cfg.GetSection(pksSection)
	if err != nil {
		return nil
	}

	if k, _ := section.GetKey("host"); k != nil {
		p.Host = k.String()
	}

	if k, _ := section.GetKey("port"); k != nil {
		port, err := k.Uint()
		if err != nil {
			return err
		}
		p.Port = port
	}
	return nil
}

func (l *Logging) parse(cfg *ini.File) error {

	section, err := cfg.GetSection(loggingSection)
	if err != nil {
		return nil
	}

	if s, _ := section.GetKey("file"); s != nil {
		file, err := ExpandPath(s.String())
		if err != nil {
			return err
		}
		l.File = file
	}

	if s, _ := section.GetKey("level"); s != nil {
		l.Level = log.ParseLogLevel(s.String())
	}

	return nil
}

func (t *TLS) parse(cfg *ini.File) error {

	section, err := cfg.GetSection(tlsSection)
	if err != nil {
		return nil
	}

	if s, _ := section.GetKey("crt"); s != nil {
		crt, err := ExpandPath(s.String())
		if err != nil {
			return err
		}
		t.CertFile = crt
	}

	if s, _ := section.GetKey("key"); s != nil {
		key, err := ExpandPath(s.String())
		if err != nil {
			return err
		}
		t.KeyFile = key
	}

	return nil
}

func (d *DB) parse(cfg *ini.File) error {

	section, err := cfg.GetSection(dbSection)
	if err != nil {
		return nil
	}

	if k, _ := section.GetKey("host"); k != nil {
		host := k.String()
		d.Host = &host
	}

	if k, _ := section.GetKey("port"); k != nil {
		port, err := k.Uint()
		if err != nil {
			return err
		}
		d.Port = &port
	}

	if k, _ := section.GetKey("name"); k != nil {
		d.Name = k.String()
	}

	if k, _ := section.GetKey("user"); k != nil {
		user := k.String()
		d.User = &user
	}

	if k, _ := section.GetKey("password"); k != nil {
		pw := k.String()
		d.Password = &pw
	}

	if k, _ := section.GetKey("ssl-mode"); k != nil {
		sl := k.String()
		d.SSLMode = &sl
	}

	return nil
}

func (c *Config) parse(cfg *ini.File) error {
	if cfg == nil {
		return nil
	}

	for _, p := range []parser{&c.Logging, &c.PKS, &c.TLS, &c.DB} {
		if err := p.parse(cfg); err != nil {
			return err
		}
	}

	return nil
}

func LoadConfig(filenames ...string) (*Config, error) {

	config := Config{
		Logging: DefaultLogging,
		PKS:     DefaultPKS,
		TLS:     DefaultTLS,
		DB:      DefaultDB,
	}

	fixedLogDefaultPath, err := ExpandPath(config.Logging.File)
	if err != nil {
		return nil, err
	}
	config.Logging.File = fixedLogDefaultPath

	continueErr := errors.New("continue")

next:
	for _, filename := range filenames {
		name, err := ExpandPath(filename)
		if err != nil {
			return nil, err
		}
		switch err := func(name string) error {
			f, err := os.Open(name)
			if err != nil {
				return continueErr
			}
			defer f.Close()

			cfg, err := ini.Load(f)
			if err != nil {
				return err
			}

			return config.parse(cfg)

		}(name); {
		case err == continueErr:
			continue next
		case err != nil:
			return nil, err
		}
		break
	}

	return &config, nil
}
