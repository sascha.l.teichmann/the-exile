// This is Free Software under GNU Affero General Public License v >= 3.0
// without warranty, see README.md and license for details.
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// License-Filename: LICENSES/AGPL-3.0.txt
//
// Copyright (C) 2021 by Intevation GmbH
//
// Author(s):
// * Sascha L. Teichmann <sascha.teichmann@intevation.de>

package database

import (
	"database/sql"
	"fmt"
	"strings"
	"unicode"

	_ "github.com/lib/pq"

	"gitlab.com/sascha.l.teichmann/the-exile/pkg/exile"
)

func escape(s string) string {
	s = strings.ReplaceAll(s, `'`, `\'`)
	if strings.IndexFunc(s, unicode.IsSpace) != -1 {
		return "'" + s + "'"
	}
	return s
}

func DBFromConfig(cfg *exile.Config) (*sql.DB, error) {

	var host string
	if cfg.DB.Host != nil {
		host = fmt.Sprintf(" host=%s", escape(*cfg.DB.Host))
	}

	var port string
	if cfg.DB.Port != nil {
		port = fmt.Sprintf(" port=%d", *cfg.DB.Port)
	}

	var user string
	if cfg.DB.User != nil {
		user = fmt.Sprintf(" user=%s", escape(*cfg.DB.User))
	}

	var pw string
	if cfg.DB.Password != nil {
		pw = fmt.Sprintf(" password=%s", escape(*cfg.DB.Password))
	}

	var sm string
	if cfg.DB.SSLMode != nil {
		pw = fmt.Sprintf(" sslmode=%s", escape(*cfg.DB.SSLMode))
	}

	con := fmt.Sprintf("dbname=%s"+
		"%s%s%s%s%s",
		escape(cfg.DB.Name),
		host, port,
		user, pw, sm)

	db, err := sql.Open("postgres", con)
	if err != nil {
		return nil, err
	}

	if err := db.Ping(); err != nil {
		db.Close()
		return nil, err
	}
	return db, nil

}
