CREATE TABLE keys (
    fingerprint TEXT NOT NULL PRIMARY KEY,
    ctime TIMESTAMP WITH TIME ZONE NOT NULL,
    mtime TIMESTAMP WITH TIME ZONE NOT NULL,
    md5 TEXT NOT NULL UNIQUE
);

CREATE TABLE subkeys (
    subfp TEXT NOT NULL PRIMARY KEY,
    fingerprint TEXT NOT NULL,
    FOREIGN KEY (fingerprint) REFERENCES keys(fingerprint)
);

DO $$
DECLARE owner name;
BEGIN
 SELECT u.usename INTO owner
   FROM pg_database d JOIN pg_user u ON (d.datdba = u.usesysid)
   WHERE d.datname = (SELECT current_database());

 EXECUTE 'GRANT SELECT, INSERT, UPDATE, DELETE ON keys, subkeys TO '
   || quote_ident(owner);
END;
$$;
