// This is Free Software under GNU Affero General Public License v >= 3.0
// without warranty, see README.md and license for details.
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// License-Filename: LICENSES/AGPL-3.0.txt
//
// Copyright (C) 2021 by Intevation GmbH
//
// Author(s):
// * Sascha L. Teichmann <sascha.teichmann@intevation.de>

package database

import (
	"context"
	"database/sql"
	"embed"
	"fmt"
	"io/fs"
	"math"
	"sort"
	"strconv"
	"unicode"
	"unicode/utf8"

	"gitlab.com/sascha.l.teichmann/the-exile/pkg/exile"
	"gitlab.com/sascha.l.teichmann/the-exile/pkg/log"
)

//go:embed migrations/*.sql
var migrations embed.FS

func num(s string) int {
	var l int
	c := s
	for len(c) > 0 {
		r, w := utf8.DecodeRuneInString(c)
		if !unicode.IsDigit(r) {
			break
		}
		l += w
		c = c[w:]
	}
	if l == 0 {
		return 0
	}
	x, _ := strconv.Atoi(s[:l])
	return x
}

func sortByNumber(entries []fs.DirEntry) {
	sort.Slice(entries, func(i, j int) bool {
		return num(entries[i].Name()) < num(entries[j].Name())
	})
}

func nextMigration(entries []fs.DirEntry, version int) int {
	for i, entry := range entries {
		if num(entry.Name()) > version {
			return i
		}
	}
	return -1
}

func highestMigrationVersion() (int, error) {
	entries, err := migrations.ReadDir("migrations")
	if err != nil {
		return 0, err
	}
	max := math.MinInt32
	for _, entry := range entries {
		if v := num(entry.Name()); v > max {
			max = v
		}
	}
	return max, nil
}

const selectMaxVersionSQL = `SELECT max(version) FROM versions`

func fetchMigrationVersion(db *sql.DB) (int, error) {
	var version sql.NullInt64
	ctx := context.Background()
	switch err := db.QueryRowContext(ctx, selectMaxVersionSQL).Scan(&version); {
	case err == sql.ErrNoRows:
		return math.MinInt32, nil
	case err != nil:
		return 0, err
	case !version.Valid:
		return math.MinInt32, nil
	}
	return int(version.Int64), nil
}

func CheckMigrationVersion(db *sql.DB) error {

	dbVersion, err := fetchMigrationVersion(db)
	if err != nil {
		return err
	}

	maxVersion, err := highestMigrationVersion()
	if err != nil {
		return err
	}

	if maxVersion != dbVersion {
		return fmt.Errorf(
			"database schema has wrong version %d. Expected version %d.",
			dbVersion, maxVersion)
	}
	return nil
}

func initVersioning(ctx context.Context, tx *sql.Tx) error {
	if _, err := tx.ExecContext(ctx,
		"CREATE TABLE IF NOT EXISTS versions (version int PRIMARY KEY)"); err != nil {
		return err
	}
	// The unpriviledged user needs to check if the version in
	// the database is current so she needs a select grant.
	_, err := tx.ExecContext(ctx, `
DO $$
DECLARE owner name;
BEGIN
 SELECT u.usename INTO owner
   FROM pg_database d JOIN pg_user u ON (d.datdba = u.usesysid)
   WHERE d.datname = (SELECT current_database());
 EXECUTE 'GRANT SELECT ON versions TO ' || quote_ident(owner);
END;
$$`)
	return err
}

func Migrate(cfg *exile.Config) error {

	entries, err := migrations.ReadDir("migrations")
	if err != nil {
		return err
	}

	if len(entries) == 0 {
		log.Infoln("No migrations found.")
		return nil
	}

	sortByNumber(entries)

	db, err := DBFromConfig(cfg)
	if err != nil {
		return err
	}

	ctx := context.Background()

	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		return nil
	}
	defer tx.Rollback()

	if err := initVersioning(ctx, tx); err != nil {
		return err
	}

	var version sql.NullInt64

	switch err := tx.QueryRowContext(
		ctx, selectMaxVersionSQL).Scan(&version); {
	case err == sql.ErrNoRows:
		version.Int64 = math.MinInt32
	case err != nil:
		return err
	case !version.Valid:
		version.Int64 = math.MinInt32
	}

	next := nextMigration(entries, int(version.Int64))
	if next == -1 {
		log.Infoln("Nothing to migrate.")
		return nil
	}

	entries = entries[next:]

	ins, err := tx.PrepareContext(
		ctx, "INSERT INTO versions (version) VALUES ($1)")
	if err != nil {
		return err
	}
	defer ins.Close()

	for _, entry := range entries {
		log.Infof("Apply migration '%s'.\n", entry.Name())
		sql, err := migrations.ReadFile(
			fmt.Sprintf("migrations/%s", entry.Name()))
		if err != nil {
			return err
		}
		if _, err := tx.ExecContext(ctx, string(sql)); err != nil {
			return err
		}
		if _, err := ins.ExecContext(ctx, num(entry.Name())); err != nil {
			return err
		}
	}

	return tx.Commit()
}
