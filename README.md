# The Exile ... Schlüsselmacher

**WIP**: A new OpenPGP keyserver.

## Development goals

- Allow for per server filters to prevent malicious keys (containing
  illegal content or attacking valid certificates with bogus
  signatures)

- Key synchronization based on modern peer-to-peer concepts with
  differing sets of keys on different servers in mind.

## Build and run

Build by invoking
```sh
make
```
this creates a new binary `exiled` in the root directory of the source
tree.  `exiled` can be directly executed locally.  When the server is
running direct your browser to <http://localhost:11371/>

A release version can be build with:
```sh
make RELEASE=1
```

## Configuration

The file `conf/exilerc` contains a example configuration.  `exiled`
searches on some default locations for the configuration file, a
specific file can also be specified using command line options.

For details see the output of
```sh
./exiled --help
```

## License

**The Exile** is licensed as Free Software under GNU Affero GPL v>=3.
See the specific  source files for details. The license
itself can be found [here](./LICENSES/AGPL-3.0.txt).
