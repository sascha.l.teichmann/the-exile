# Database setup

**The Exile** uses a PostgreSQL database to store its data in.
You have to install a database cluster, create a user and a database in it.

On a Debian system this might look something like this.
Run as `root` user:

```sh
apt-get install postgresql
su - postgres
createuser exile --pwpromt
# Enter a password twice say e.g. SECRET
createdb exiledb --owner exile
```

Adjust the PostgreSQL configuration to allow access from the
locations you want to use it from.

The database has to be initialised. You have to use the
`exiled` program to do the needed migrations.

Create a special configuration file to connect to the
database for this purpose. It is needed to run the
migrations as privileged database user.

Example `migrate.ini` file:
```dosini
[logging]
file=/tmp/exiled-migrate.txt
[db]
host=/var/run/postgresql
```

Run as `postgres` user:
```sh
exiled --migrate --config `readlink -f migrate.ini`
```
