# Generate public/private key pair for test purposes

## Generate private key (.key)

```sh
# Key considerations for algorithm "RSA" ≥ 2048-bit
openssl genrsa -out server.key 2048

# Key considerations for algorithm "ECDSA" ≥ secp384r1
# List ECDSA the supported curves (openssl ecparam -list_curves)
openssl ecparam -genkey -name secp384r1 -out server.key
```

## Generate self-signed public key (.crt) based on the private key (.key)

```sh
openssl req -new -x509 -sha256 -key server.key -out server.crt -days 3650
```
